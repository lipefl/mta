# Chronicles of Darkness 2e for FoundryVTT (former: Mage the Awakening)
Non-official (fan-made) system.

- Manifest: https://gitlab.com/MarlQ/mta/-/raw/master/system.json
- Releases: https://gitlab.com/MarlQ/mta/-/releases
- Bugs & feature requests: https://gitlab.com/MarlQ/mta/-/issues
- Currently working on: https://gitlab.com/MarlQ/mta/-/milestones
- Foundry System page: https://foundryvtt.com/packages/mta

For contact: SoulCake#7804 in Discord or the wod channel in the FoundryVTT server

If someone wants to help with development, you're always welcome.  
I'll continue working on this until I'm done with my current campaign,  
though if people are interesting I might continue afterwards.  

Dev branch will possibly be unstable, progress branch is for my personal homebrew and the place where most features originate.  
If you want to make a merge request, please use the `dev` branch (if you can be bothered).

If you want to support the project, feel free to [buy me a coffee](https://ko-fi.com/soulcake) :)

Also check out my self-made TAC system (currently on hiatus): https://gitlab.com/MarlQ/tac

## Features

### Images (click for full-size)

[<img src="/screenshots/characterSheet.png" width="168" height="128">](/screenshots/characterSheet.png)
[<img src="/screenshots/improvisedSpellcasting.png" width="128" height="128">](/screenshots/improvisedSpellcasting.png)
[<img src="/screenshots/werewolfSheet.png" width="128" height="128">](/screenshots/werewolfSheet.png)

### Supported Sheets
* Mortals (CofD)
* Mages (MtAw 2e)
* Vampires (VtR 2e)
* Changelings (CtL 2e)
* Werewolf (WtF 2e)
* Demons (DtD)
* Sleepwalkers (MtAw 2e)
* Proximi (MtAw 2e)
* Ephemeral Entities (Angels, Ghosts, Spirits)

### Current Features

* Sheets for Spells, Active Spells, Attainments, Yantras, Firearms, Melee, Armor, Ammo, Equipment, Services, Merits, Conditions, Tilts, ...
* Tracking of pretty much all the things the original character sheet contains
* Rolling of any combination of Attribute, Skill, and other trait
* Complete dice roller with all rules (e.g. 10-/9-/8-again, rote pools, chance die removing 10-again, and 1-again for rote pools)
* Dice penalties for 0 dots in Skills
* Intelligent health tracking (kudos to Pecklaaz)
* Reloading system from my TAC System (might be a bit over-the-top but it works), in order to reload you have to have ammo your inventory with the same cartridge as the firearm
* Basic Doors/Impression tracking
* Quick calculation of derived stats
* Weapon rolling
* Mage: (Improvised) spell casting! With almost all features as the one on voidstate.com (except Yantras)!
* Beat tracking system (with history, and reasons, etc.)
* Buff any trait via Tilts, Conditions, or Merits
* Combining any dice pool with an item
..and much more!

### Localisation

- English
- French by user lowkey
I know the localisation is very inconsistent (because I hate working on it).

### License
All rights reserved. 
However, I'm usually pretty open when asked.
If I ever stop working on the project, feel free to do whatever you want with the code.

### Credits

* Source books created by White Wolf / Paradox (Dark pact: https://www.worldofdarkness.com/dark-pack)
* Icons from https://game-icons.net/ (License: https://creativecommons.org/licenses/by/3.0/)
* Icons from fontawesome (License: https://fontawesome.com/license)
* All other icons/textures are CC0
* Some code snippets were used from the dnd 5e system.
* Thanks to Pecklaaz for some of the GUI work.
